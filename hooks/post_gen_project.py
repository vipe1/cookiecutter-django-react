import os

project_path = os.getcwd()


def set_permissions_for_shell_script(mode):
    for root, dirs, files in os.walk(project_path):
        for file in files:
            file_path = os.path.join(root, file)
            if file_path.endswith('.sh'):
                os.chmod(file_path, mode)


def main():
    set_permissions_for_shell_script(0o755)


if __name__ == '__main__':
    main()
