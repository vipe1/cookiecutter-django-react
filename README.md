# Cookiecutter Django React

## Components:
* Django container:
  * DRF
  * Ruff, Black and isort
  * Pytest
* React container:
  * Vite
  * Mantine
* Postgres container

## Usage

Set up the project with:
```shell
$ pip install cookiecutter
$ cookiecutter https://gitlab.com/vipe1/cookiecutter-django-react/
```

Install additional prerequisites:
```shell
$ pip install ruff black isort
```

Don't forget to run `./backend/command.sh migrate` to set up database
