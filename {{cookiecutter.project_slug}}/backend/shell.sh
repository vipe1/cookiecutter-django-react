#!/usr/bin/env bash

cd "$(git rev-parse --show-toplevel)" || exit
./backend/command.sh shell_plus "$@"
