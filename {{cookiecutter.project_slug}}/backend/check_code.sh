#!/usr/bin/env bash

cd "$(git rev-parse --show-toplevel)" || exit

cd backend || exit
echo "Checking backend code 🛠️"

ruff check . &&
black --check . &&
isort --check .

exit $?