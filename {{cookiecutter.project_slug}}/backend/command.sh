#!/usr/bin/env bash

cd "$(git rev-parse --show-toplevel)" || exit
docker compose run --rm backend python manage.py "$@"