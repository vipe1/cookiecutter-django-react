from config.settings.common import *

INSTALLED_APPS += [
    'django_extensions',
]

ALLOWED_HOSTS = ['*']
CSRF_TRUSTED_ORIGINS = ['http://localhost:3000']

SHELL_PLUS = 'ipython'
