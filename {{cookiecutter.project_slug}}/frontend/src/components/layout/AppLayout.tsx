import { AppShell, Burger, Group, Title } from '@mantine/core'
import { useDisclosure } from '@mantine/hooks'
import React from 'react'

interface AppLayoutProps {
    children: React.ReactNode
}

const AppLayout = ({ children }: AppLayoutProps ) => {
    const [opened, { toggle }] = useDisclosure()

{% raw %}
    return (
        <AppShell
            padding={'xs'}
            header={{ height: 60 }}
            navbar={{ width: 300, breakpoint: 'sm', collapsed: { mobile: !opened } }}
        >
{% endraw %}
            <AppShell.Header>
                <Group h={'100%'}>

                    <Burger opened={opened} onClick={toggle} hiddenFrom="sm" size="sm"/>
                    <Title>{{cookiecutter.project_name}}</Title>
                </Group>
            </AppShell.Header>

            <AppShell.Navbar>
                Navbar
            </AppShell.Navbar>

            <AppShell.Main>
                {children}
            </AppShell.Main>
        </AppShell>
    )
}

export default AppLayout
