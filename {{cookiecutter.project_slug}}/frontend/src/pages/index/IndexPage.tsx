import { Outlet } from 'react-router-dom'
import AppLayout from '@components/layout/AppLayout.tsx'

const IndexPage = () => {
    return (
        <AppLayout>
            <Outlet/>
        </AppLayout>
    )
}

export default IndexPage