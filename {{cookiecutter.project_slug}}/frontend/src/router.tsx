import { createBrowserRouter, createRoutesFromElements, Route } from 'react-router-dom'
import IndexPage from './pages/index/IndexPage.tsx'


export const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path='/' element={<IndexPage/>}>
        </Route>
    )
)